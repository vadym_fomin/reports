package com.flexSolution.jasperReports;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionProvider {

    public static Connection getConnection()
    {
        Connection connection = null;
        try
        {
//            Class.forName("com.mysql.jdbc.Driver");
            String dbURL = "jdbc:mysql://192.168.0.101:3306/tproc?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";
            connection = DriverManager.getConnection(dbURL,"root","F!exROOT#2015");
        }
        catch(Exception exception)
        {
            exception.printStackTrace();
        }
        return connection;

    }
}
