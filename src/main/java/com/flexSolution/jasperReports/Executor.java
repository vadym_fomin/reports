package com.flexSolution.jasperReports;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import java.io.File;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class Executor {

    private static void generateReport(String templatePath, String reportPath) throws JRException, ParseException {
        File reportPattern = new File(ClassLoader.getSystemResource(templatePath).getPath());
        JasperDesign jasperDesign = JRXmlLoader.load(reportPattern);
        JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
        Connection connection = ConnectionProvider.getConnection();
        HashMap<String, Object> parameters = new HashMap<>();
        parameters.put("REPORT_CONNECTION", connection);
        parameters.put("LOGO", ClassLoader.getSystemResource("img/doc_proc_dark.svg").getPath());
        parameters.put("CURRENT_DATE", new Date());
        parameters.put("START_DATE","2018-10-11");
        parameters.put("END_DATE","2018-10-12");
        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport,
                parameters, new JREmptyDataSource());
        JasperExportManager.exportReportToPdfFile(jasperPrint, reportPath);
        System.out.println("Report was created");
    }

    public static void main(String[] args) throws JRException, ParseException {
//        generateReport("templates/test.jrxml", "src/main/resources/output/templates.pdf");
        generateReport("templates/users.jrxml", "src/main/resources/output/users.pdf");
    }
}
